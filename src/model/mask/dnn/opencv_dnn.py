import cv2
import numpy as np

from .anchor_decode import decode_bbox
from .anchor_generator import generate_anchors
from .nms import single_class_non_max_suppression
# anchor configuration
from ..mask_detector import MaskDetector

feature_map_sizes = [[33, 33], [17, 17], [9, 9], [5, 5], [3, 3]]
anchor_sizes = [[0.04, 0.056], [0.08, 0.11], [0.16, 0.22], [0.32, 0.45], [0.64, 0.72]]
anchor_ratios = [[1, 0.62, 0.42]] * 5

# generate anchors
anchors = generate_anchors(feature_map_sizes, anchor_sizes, anchor_ratios)

# for inference , the batch size is 1, the model output shape is [1, N, 4],
# so we expand dim for anchors to [1, anchor_num, 4]
anchors_exp = np.expand_dims(anchors, axis=0)

id2class = {0: 'Mask', 1: 'NoMask'}
colors = ((0, 255, 0), (255, 0, 0))


class CV2MaskDetector(MaskDetector):
    def __init__(self, config):
        super().__init__(config)
        self.model = cv2.dnn.readNet(config.checkpoint, config.proto)

        layersNames = self.model.getLayerNames()
        self.outputs = [layersNames[i[0] - 1] for i in self.model.getUnconnectedOutLayers()]

    def predict(self, image, conf_thresh=0.5, iou_thresh=0.4, target_shape=(160, 160), draw_result=True, **kwargs):
        height, width, _ = image.shape
        blob = cv2.dnn.blobFromImage(image, scalefactor=1 / 255.0, size=target_shape)

        self.model.setInput(blob)
        y_bboxes_output, y_cls_output = self.model.forward(self.outputs)

        # remove the batch dimension, for batch is always 1 for inference.
        y_bboxes = decode_bbox(anchors_exp, y_bboxes_output)[0]
        y_cls = y_cls_output[0]
        # To speed up, do single class NMS, not multiple classes NMS.
        bbox_max_scores = np.max(y_cls, axis=1)
        bbox_max_score_classes = np.argmax(y_cls, axis=1)

        # keep_idx is the alive bounding box after nms.
        keep_idxs = single_class_non_max_suppression(y_bboxes, bbox_max_scores, conf_thresh=conf_thresh,
                                                     iou_thresh=iou_thresh)
        # keep_idxs  = cv2.dnn.NMSBoxes(y_bboxes.tolist(), bbox_max_scores.tolist(), conf_thresh, iou_thresh)[:,0]
        tl = round(0.002 * (height + width) * 0.5) + 1  # line thickness
        for idx in keep_idxs:
            conf = float(bbox_max_scores[idx])
            class_id = bbox_max_score_classes[idx]
            bbox = y_bboxes[idx]
            # clip the coordinate, avoid the value exceed the image boundary.
            xmin = max(0, int(bbox[0] * width))
            ymin = max(0, int(bbox[1] * height))
            xmax = min(int(bbox[2] * width), width)
            ymax = min(int(bbox[3] * height), height)
            if draw_result:
                cv2.rectangle(image, (xmin, ymin), (xmax, ymax), colors[class_id], thickness=tl)
                cv2.putText(image, "%s: %.2f" % (id2class[class_id], conf), (xmin + 2, ymin - 2),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.8, colors[class_id])
        return image
