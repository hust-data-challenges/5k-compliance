import click

import src.utils as utils
from src import tasks
from src.dataset import get_dataloader
from src.loss import get_criterion
from src.model import get_model
from src.optimizer import get_optimizer
from src.scheduler import get_scheduler
from src.transforms import get_transform, function
from src.utils.logger import get_logger
from src.utils.writer import MetricWriter

logger = get_logger(__name__)


@click.command()
@click.option("--config_file", required=True, type=str)
@click.option("--expr_name", required=True, type=str)
@click.option("--expr_ver", default=None, type=str)
def run(config_file, expr_name, expr_ver):
    config = utils.config.load(config_file)

    if expr_ver is not None:
        config.train.dir = f"{config.train.dir}.{expr_ver}"

    logger.info(f"Run {expr_name} experiment logged {config.train.dir} with {expr_ver}")
    logger.info(config)

    utils.misc.prepare_train_directories(config)

    model = get_model(config)
    optimizer = get_optimizer(config, model.parameters())
    checkpoint = utils.checkpoint.get_initial_checkpoint(config)

    last_epoch, step = -1, -1
    if checkpoint:
        last_epoch, step = utils.checkpoint.load_checkpoint(model, optimizer, checkpoint)

    scheduler = get_scheduler(config, optimizer, last_epoch)
    preprocess_opt = function.get_preprocess_opt(config, model=model)
    loaders = {split: get_dataloader(config, split, get_transform(config, split, **preprocess_opt))
               for split in ['train', 'val']}

    criterion = get_criterion(config)
    writer = MetricWriter(f"{config.train.dir}/log")

    if expr_name == "mask_detection":
        tasks.mask_train.mask_detection(config, model, loaders, optimizer, scheduler, criterion, writer, last_epoch)
        return

    if expr_name == "gather_detection":
        tasks.gather_train.mask_detection(config, model, loaders, optimizer, scheduler, criterion, writer, last_epoch)
        return

    if expr_name == "face_detection":
        tasks.face_train.mask_detection(config, model, loaders, optimizer, scheduler, criterion, writer, last_epoch)
        return


if __name__ == "__main__":
    run()
