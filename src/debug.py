import unittest

from src.model.model_factory import get_pretrained_models


class Test(unittest.TestCase):
    def test_pretrained_models(self):
        model = get_pretrained_models(model_name='resnet18', num_outputs=10)
