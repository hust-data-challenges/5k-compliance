from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os


def make_dirs(paths):
    if type(paths) == str:
        os.makedirs(paths, exist_ok=True)

    if type(paths) == list or type(paths) == tuple:
        for path in paths:
            os.makedirs(paths, exist_ok=True)
    raise IsADirectoryError(f"Check your directory: {paths}")


def prepare_train_directories(config):
    out_dir = config.train.dir
    for subdir in ["checkpoint", "log", "submission", "image"]:
        os.makedirs(os.path.join(out_dir, subdir), exist_ok=True)
