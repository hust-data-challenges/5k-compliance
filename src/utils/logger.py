import logging
import logging.config
import os

from multiprocessing_logging import install_mp_handler

mode = os.environ.get('5k_COMPLIANCE', 'DEV')

if mode == "DEV":
    # default is dev
    app_name = os.environ.get('APP_NAME', 'DEV_APP')

    print(f"Using dev config with APP_NAME = {app_name}")
    logging.app_name = app_name

    # create log dir
    log_dir = os.path.join(os.path.dirname(__file__), f"../log/{logging.app_name}")
    os.makedirs(log_dir, exist_ok=True)

    # setting log_file
    logging.log_file = os.path.join(log_dir, f"{app_name}.log")
    logging.log_file_error = os.path.join(log_dir, f"{app_name}_error.log")

    logging.config.fileConfig(os.path.join(os.path.dirname(__file__), '../logging_dev.ini'))
    install_mp_handler()

elif mode == 'PROD':
    if 'APP_NAME' not in os.environ:
        raise Exception("APP_NAME environment variable must be set in production")

    app_name = os.environ['APP_NAME']
    logging.app_name = app_name

    # create log dir
    log_dir = os.path.join(f"/data/log/{app_name}")
    os.makedirs(log_dir, exist_ok=True)

    # setting log file
    logging.log_file = os.path.join(log_dir, f'{app_name}.log')
    logging.log_file_error = os.path.join(log_dir, f'{app_name}_error.log')

    print("Using prod config")
    logging.config.fileConfig(os.path.join(os.path.dirname(__file__), '../logging.ini'))

    # install multiprocessing logger
    # only works in linux
    print("Setting distributed logging")
    install_mp_handler()

elif mode == 'JUPYTER' or mode == 'TEST':
    # do nothing
    pass


def get_logger(name):
    logger = logging.getLogger(name)
    return logger


def get_multi_process_logger(name):
    """
    Get multiprocessing logger
    :param name:
    :return:
    """
    logger = logging.getLogger(name)
    return logger
